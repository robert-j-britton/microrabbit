﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroRabbit.Banking.Application.Models
{
    public class AccountTransfer
    {
        public int AccountSource { get; set; }
        public int AccountDestination { get; set; }
        public decimal TransferAmount { get; set; }
    }
}
